#!/bin/bash

export HOME=/home/ubuntu

echo "Performing weekly backup and deleting 90+ day old snapshots"

ntpdate us.pool.ntp.org
/usr/local/aws-missing-tools/ec2-automate-backup/ec2-automate-backup-awscli.sh -r us-west-2 -s tag -t 'Backup-weekly,Values=true' -k 90 -p -n
