# Matrushka EC2 tools

## Enabling EC2 weekly backups

**Requirements**

* Git installed `sudo apt-get install git`
* Multiverse enabled `sudo vim /etc/apt/sources.list`, uncomment the *multiverse* lines.
* AWS-missing-tools installed `sudo bash install-aws-missing-tools.sh`
* Configure the user AWS credentials with `aws configure`

With the requirements installed, you just need to: 

* Add the cron job by running `sudo bash install-cron-weekly-backup.sh` 
* Tag the volumes you want to want to snapshot every week with *Backup-weekly* set to *true*

To fix the `A client error (AuthFailure) occurred when calling the DescribeRegions operation: AWS was not able to validate the provided access credentials  to sync` added the following line to the script `ntpdate us.pool.ntp.org` the second parameter can be any ntp server. 

Get AWS credentials https://console.aws.amazon.com/iam/home?#users

Configure AWS http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html